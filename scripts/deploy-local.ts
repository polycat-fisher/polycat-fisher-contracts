import { BigNumber } from "ethers";
import { ethers } from "hardhat";

async function main() {
  const INITIAL_FISH_SUPPLY = BigNumber.from("10000000000000000000000");
  const INITIAL_PAW_SUPPLY = BigNumber.from("10000000000000000000000");
  const POOL_INDEX = 0x0;

  const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
  const CatRouterMock = await ethers.getContractFactory("CatRouterMock");
  const StrategyDividendsMock = await ethers.getContractFactory(
    "StrategyDividendsMock",
  );
  const VaultChefMock = await ethers.getContractFactory("VaultChefMock");
  const PolycatFishCompounder = await ethers.getContractFactory(
    "PolycatFishCompounder",
  );

  const fishToken = await ERC20Mock.deploy(INITIAL_FISH_SUPPLY, "Fish", "FISH");
  const pawToken = await ERC20Mock.deploy(INITIAL_PAW_SUPPLY, "Paw", "PAW");

  await fishToken.deployed();
  await pawToken.deployed();

  const catRouter = await CatRouterMock.deploy(
    fishToken.address,
    pawToken.address,
  );
  const strategyDividends = await StrategyDividendsMock.deploy(
    pawToken.address,
  );
  const vaultChef = await VaultChefMock.deploy(fishToken.address);

  await catRouter.deployed();
  await strategyDividends.deployed();
  await vaultChef.deployed();

  const polycatCompounder = await PolycatFishCompounder.deploy(
    fishToken.address,
    pawToken.address,
    vaultChef.address,
    catRouter.address,
    strategyDividends.address,
    POOL_INDEX,
  );

  await polycatCompounder.deployed();

  console.log(
    `FISH deployed to ${fishToken.address}\n` +
      `PAW deployed to ${pawToken.address}\n` +
      `CatRouter deployed to ${catRouter.address}\n` +
      `StrategyDividends deployed to ${strategyDividends.address}\n` +
      `VaultChef deployed to ${vaultChef.address}\n` +
      `PolycatCompounder deployed to ${polycatCompounder.address}\n`,
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
