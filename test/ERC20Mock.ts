import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

const INITIAL_BALANCE = 1_000_000_000;
const NAME = "Mock";
const SYMBOL = "MOCK";

describe("ERC20Mock", () => {
  const deployERC20MockFixture = async () => {
    const [owner, otherAccount] = await ethers.getSigners();

    const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    const erc20Mock = await ERC20Mock.deploy(INITIAL_BALANCE, NAME, SYMBOL);

    return { erc20Mock, owner, otherAccount };
  };

  describe("Deployment", () => {
    it("Should set the right token attributes", async () => {
      const { erc20Mock } = await loadFixture(deployERC20MockFixture);

      expect(await erc20Mock.name()).to.equal(NAME);
      expect(await erc20Mock.symbol()).to.equal(SYMBOL);
      expect(await erc20Mock.totalSupply()).to.equal(INITIAL_BALANCE);
    });

    it("Should set the right balance to deployer account", async () => {});
  });

  describe("Transfer", () => {
    it("Should allow transfers between accounts", async () => {});
  });
});
