import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";
import {
  CatRouterMock,
  ERC20Mock,
  PolycatFishCompounder,
  StrategyDividendsMock,
  VaultChefMock,
} from "../typechain-types";

const INITIAL_BALANCE = 1_000_000_000;
const POOL_INDEX = 0x103;

type TestingContext = {
  owner: { address: string };
  compounder: { address: string };
  stranger: { address: string };
  polycatCompounder: PolycatFishCompounder;
  fishToken: ERC20Mock;
  pawToken: ERC20Mock;
  catRouter: CatRouterMock;
  strategyDividends: StrategyDividendsMock;
  vaultChef: VaultChefMock;
};

describe("PolycatFishCompounder", () => {
  const deployTestingContextFixture = async (): Promise<TestingContext> => {
    const [owner, compounder, stranger] = await ethers.getSigners();

    const ERC20Mock = await ethers.getContractFactory("ERC20Mock");
    const CatRouterMock = await ethers.getContractFactory("CatRouterMock");
    const StrategyDividendsMock = await ethers.getContractFactory(
      "StrategyDividendsMock",
    );
    const VaultChefMock = await ethers.getContractFactory("VaultChefMock");
    const PolycatFishCompounder = await ethers.getContractFactory(
      "PolycatFishCompounder",
    );

    const fishToken = await ERC20Mock.deploy(INITIAL_BALANCE, "Fish", "FISH");
    const pawToken = await ERC20Mock.deploy(INITIAL_BALANCE, "Paw", "PAW");

    const catRouter = await CatRouterMock.deploy(
      fishToken.address,
      pawToken.address,
    );
    const strategyDividends = await StrategyDividendsMock.deploy(
      pawToken.address,
    );
    const vaultChef = await VaultChefMock.deploy(fishToken.address);

    const polycatCompounder = await PolycatFishCompounder.deploy(
      fishToken.address,
      pawToken.address,
      vaultChef.address,
      catRouter.address,
      strategyDividends.address,
      POOL_INDEX,
    );

    return {
      owner,
      compounder,
      stranger,
      polycatCompounder,
      fishToken,
      pawToken,
      catRouter,
      strategyDividends,
      vaultChef,
    };
  };

  describe("Deployment", () => {
    it("Should set the right states variables", async () => {
      const {
        owner,
        polycatCompounder,
        fishToken,
        pawToken,
        catRouter,
        strategyDividends,
        vaultChef,
      }: TestingContext = await loadFixture(deployTestingContextFixture);

      const [
        ownerQuery,
        compounderQuery,
        fishQuery,
        pawQuery,
        routerQuery,
        chefQuery,
        dividendsQuery,
      ] = await Promise.all([
        polycatCompounder.owner(),
        polycatCompounder.compounder(),
        polycatCompounder.FISH(),
        polycatCompounder.PAW(),
        polycatCompounder.router(),
        polycatCompounder.chef(),
        polycatCompounder.dividends(),
      ]);

      expect(ownerQuery).to.equal(owner.address);
      expect(compounderQuery).to.equal(owner.address);
      expect(fishQuery).to.equal(fishToken.address);
      expect(pawQuery).to.equal(pawToken.address);
      expect(routerQuery).to.equal(catRouter.address);
      expect(chefQuery).to.equal(vaultChef.address);
      expect(dividendsQuery).to.equal(strategyDividends.address);
    });

    it("Should emit CompounderSet event", async () => {
      const { owner, polycatCompounder }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      expect(polycatCompounder.deployTransaction).to.emit(
        polycatCompounder,
        "CompounderSet",
      );
    });
  });

  describe("Compounder", () => {
    it("Should emit CompounderSet event", async () => {});

    it("Should set compounder", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      expect(await context.polycatCompounder.compounder()).to.equal(
        context.owner.address,
      );

      await context.polycatCompounder.setCompounder(context.compounder.address);

      expect(await context.polycatCompounder.compounder()).to.equal(
        context.compounder.address,
      );
    });

    it("Should allow compounder to compound", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      await context.polycatCompounder.setCompounder(context.compounder.address);
    });

    it("Should not allow stranger to compound", async () => {});
  });

  describe("Holdings", () => {
    it("Should retrieve FISH balance", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      const balanceBefore = await context.fishToken.balanceOf(
        context.owner.address,
      );
      const balanceOfContractBefore = await context.fishToken.balanceOf(
        context.polycatCompounder.address,
      );

      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );

      const balanceAfter = await context.fishToken.balanceOf(
        context.owner.address,
      );
      const balanceOfContractAfter = await context.fishToken.balanceOf(
        context.polycatCompounder.address,
      );

      expect(await context.polycatCompounder.fishBalance()).to.equal(amount);
      expect(
        balanceOfContractAfter.toNumber() - balanceOfContractBefore.toNumber(),
      ).to.equal(amount);
      expect(balanceBefore.toNumber() - balanceAfter.toNumber()).to.equal(
        amount,
      );
    });

    it("Should retrieve PAW balance", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      const balanceBefore = await context.pawToken.balanceOf(
        context.owner.address,
      );
      const balanceOfContractBefore = await context.pawToken.balanceOf(
        context.polycatCompounder.address,
      );

      await context.pawToken.transfer(
        context.polycatCompounder.address,
        amount,
      );

      const balanceAfter = await context.pawToken.balanceOf(
        context.owner.address,
      );
      const balanceOfContractAfter = await context.pawToken.balanceOf(
        context.polycatCompounder.address,
      );

      expect(await context.polycatCompounder.pawBalance()).to.equal(amount);
      expect(
        balanceOfContractAfter.toNumber() - balanceOfContractBefore.toNumber(),
      ).to.equal(amount);
      expect(balanceBefore.toNumber() - balanceAfter.toNumber()).to.equal(
        amount,
      );
    });

    it("Should withdraw all FISH and PAW", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      const fishBalanceBefore = await context.fishToken.balanceOf(
        context.owner.address,
      );
      const pawBalanceBefore = await context.pawToken.balanceOf(
        context.owner.address,
      );

      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );
      await context.pawToken.transfer(
        context.polycatCompounder.address,
        amount,
      );

      await context.polycatCompounder.withdraw();

      expect(await context.polycatCompounder.fishBalance()).to.equal(0);
      expect(await context.polycatCompounder.pawBalance()).to.equal(0);

      expect(await context.fishToken.balanceOf(context.owner.address)).to.equal(
        fishBalanceBefore,
      );
      expect(await context.pawToken.balanceOf(context.owner.address)).to.equal(
        pawBalanceBefore,
      );
    });

    it("Should emit Withdrawn event", async () => {});
  });

  describe("Staking", () => {
    it("Should emit Staked event", async () => {});

    it("Should stake all FISH balance", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );

      await context.polycatCompounder.stake();

      expect(await context.polycatCompounder.fishBalance()).to.equal(0);
      expect(await context.polycatCompounder.depositedFishBalance()).to.equal(
        amount,
      );

      // Should left allowance to 0 for security
      expect(
        await context.fishToken.allowance(
          context.polycatCompounder.address,
          context.vaultChef.address,
        ),
      ).to.equal(0);

      expect(
        await context.vaultChef.userInfo(
          POOL_INDEX,
          context.polycatCompounder.address,
        ),
      ).to.equal(amount);
    });

    it("Should unstake all FISH balance", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      const fishBalanceBefore = await context.fishToken.balanceOf(
        context.owner.address,
      );
      const pawBalanceBefore = await context.pawToken.balanceOf(
        context.owner.address,
      );

      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );
      await context.pawToken.transfer(
        context.polycatCompounder.address,
        amount,
      );

      await context.polycatCompounder.stake();
      await context.polycatCompounder.unstakeAndWithdraw();

      expect(await context.polycatCompounder.fishBalance()).to.equal(0);
      expect(await context.polycatCompounder.depositedFishBalance()).to.equal(
        0,
      );
      expect(await context.polycatCompounder.pawBalance()).to.equal(0);

      expect(await context.fishToken.balanceOf(context.owner.address)).to.equal(
        fishBalanceBefore,
      );
      expect(await context.pawToken.balanceOf(context.owner.address)).to.equal(
        pawBalanceBefore,
      );
    });

    it("Should unstake and withdraw all FISH and PAW", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );
    });
  });

  describe("Compounding", () => {
    it("Should harvest and compound all PAW rewards", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      await context.pawToken.transfer(
        context.strategyDividends.address,
        2 * amount,
      );
      await context.fishToken.transfer(context.catRouter.address, 3 * amount);
      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );
      await context.polycatCompounder.stake();

      const pendingPaw = await context.strategyDividends.pendingPaw(
        context.polycatCompounder.address,
      );
      expect(pendingPaw).not.to.equal(0);

      const currentFish =
        await context.polycatCompounder.depositedFishBalance();
      const expectedAdditionalFish = (
        await context.catRouter.getAmountsOut(pendingPaw, [
          context.pawToken.address,
          context.fishToken.address,
        ])
      )[1];

      await context.polycatCompounder.compound();

      const newFishBalance =
        await context.polycatCompounder.depositedFishBalance();
      expect(newFishBalance).to.equal(
        currentFish.toNumber() + expectedAdditionalFish.toNumber(),
      );
    });

    it("Should retrieve the PAW/FISH quote", async () => {
      const context: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      const amount = 1_000_000;

      await context.pawToken.transfer(
        context.strategyDividends.address,
        2 * amount,
      );
      await context.fishToken.transfer(context.catRouter.address, 3 * amount);
      await context.fishToken.transfer(
        context.polycatCompounder.address,
        amount,
      );
      await context.polycatCompounder.stake();

      const pendingPaw = await context.strategyDividends.pendingPaw(
        context.polycatCompounder.address,
      );
      expect(pendingPaw).not.to.equal(0);

      const expectedFish = (
        await context.catRouter.getAmountsOut(pendingPaw, [
          context.pawToken.address,
          context.fishToken.address,
        ])
      )[1];

      expect(await context.polycatCompounder.getCompoundQuote()).to.equal(
        expectedFish,
      );
    });
  });
});
