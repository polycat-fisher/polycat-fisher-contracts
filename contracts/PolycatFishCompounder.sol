// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "./PolycatInterfaces.sol";

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "hardhat/console.sol";

// Polygon addresses:
// IERC20 PAW = IERC20(0xBC5b59EA1b6f8Da8258615EE38D40e999EC5D74F);
// IERC20 FISH = IERC20(0x3a3Df212b7AA91Aa0402B9035b098891d276572B);

// VaultChef chef = VaultChef(0xBdA1f897E851c7EF22CD490D2Cf2DAce4645A904);
// CatRouter router = CatRouter(0x94930a328162957FF1dd48900aF67B5439336cBD);
// StrategyDividends dividends =
//     StrategyDividends(0x640328B6BB1856dDB6a7d7BB07e5E1F3D9F50B94);
// uint256 POOL = 259; // 0x103

contract PolycatFishCompounder {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    address public owner;
    address public compounder;

    event CompounderSet(
        address indexed oldCompounder,
        address indexed newCompounder
    );

    event OwnerSet(address indexed oldOwner, address indexed newOwner);

    modifier isOwner() {
        require(msg.sender == owner, "Caller is not owner");
        _;
    }

    modifier canCompound() {
        require(
            msg.sender == compounder || msg.sender == owner,
            "Caller cannot compound"
        );
        _;
    }

    function setCompounder(address newCompounder) public isOwner {
        emit CompounderSet(compounder, newCompounder);
        compounder = newCompounder;
    }

    IERC20 public PAW;
    IERC20 public FISH;

    VaultChef public chef;
    CatRouter public router;
    StrategyDividends public dividends;

    uint256 public POOL;

    constructor(
        address _fishToken,
        address _pawToken,
        address _vaultChef,
        address _catRouter,
        address _strategyDividends,
        uint256 _poolIndex
    ) {
        console.log("Polycat compounder contract deployed by:", msg.sender);

        PAW = IERC20(_pawToken);
        FISH = IERC20(_fishToken);
        chef = VaultChef(_vaultChef);
        router = CatRouter(_catRouter);
        dividends = StrategyDividends(_strategyDividends);
        POOL = _poolIndex;
        compounder = msg.sender;
        owner = msg.sender;
        emit OwnerSet(address(0), owner);
        emit CompounderSet(address(0), compounder);
    }

    function changeOwner(address newOwner) public isOwner {
        emit OwnerSet(owner, newOwner);
        owner = newOwner;
    }

    function depositedFishBalance() public view returns (uint256) {
        return chef.userInfo(POOL, address(this));
    }

    function fishBalance() public view returns (uint256) {
        return FISH.balanceOf(address(this));
    }

    function pendingPawBalance() public view returns (uint256) {
        return dividends.pendingPaw(address(this));
    }

    function pawBalance() public view returns (uint256) {
        return PAW.balanceOf(address(this));
    }

    event Staked(address _owner);

    function stake() public canCompound {
        emit Staked(owner);
        FISH.approve(address(chef), fishBalance());
        chef.deposit(POOL, fishBalance());
    }

    event Unstaked(address _owner);

    function unstake() public isOwner {
        emit Unstaked(owner);
        chef.withdrawAll(POOL);
    }

    event Harvested(address _owner);

    function harvest() public {
        emit Harvested(owner);
        dividends.harvest();
    }

    event Withdrawn(address _owner);

    function withdraw() public isOwner {
        emit Withdrawn(owner);
        FISH.transfer(owner, fishBalance());
        PAW.transfer(owner, pawBalance());
    }

    function unstakeAndWithdraw() public isOwner {
        harvest();
        unstake();
        withdraw();
    }

    function getCompoundQuote() public view returns (uint256) {
        address[] memory path = new address[](2);
        path[0] = address(PAW);
        path[1] = address(FISH);
        return
            router.getAmountsOut(pendingPawBalance() + pawBalance(), path)[1];
    }

    function compound() public canCompound {
        harvest();

        address[] memory path = new address[](2);
        path[0] = address(PAW);
        path[1] = address(FISH);

        PAW.approve(address(router), pawBalance());

        router.swapExactTokensForTokens(
            pawBalance(),
            uint256(0),
            path,
            address(this),
            block.timestamp.add(1800)
        );

        stake();
    }
}
