// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "../PolycatInterfaces.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract VaultChefMock is VaultChef {
    IERC20 public fishToken;

    constructor(address _fishToken) {
        fishToken = IERC20(_fishToken);
    }

    function deposit(uint256 _pid, uint256 _wantAmt) external override {
        fishToken.transferFrom(msg.sender, address(this), _wantAmt);
        userInfo[_pid][msg.sender].shares += _wantAmt;
    }

    function withdraw(uint256 _pid, uint256 _wantAmt) external override {
        userInfo[_pid][msg.sender].shares -= _wantAmt;
        fishToken.transfer(msg.sender, _wantAmt);
    }

    function withdrawAll(uint256 _pid) external override {
        fishToken.transfer(msg.sender, userInfo[_pid][msg.sender].shares);
        userInfo[_pid][msg.sender].shares = 0;
    }
}
