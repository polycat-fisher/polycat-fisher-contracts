// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "../PolycatInterfaces.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract StrategyDividendsMock is StrategyDividends {
    IERC20 public pawToken;

    constructor(address _pawToken) {
        pawToken = IERC20(_pawToken);
    }

    function harvest() external {
        pawToken.transfer(msg.sender, pendingPaw(msg.sender));
    }

    function pendingPaw(
        address /*_user*/
    ) public view returns (uint256) {
        return pawToken.balanceOf(address(this));
    }
}
