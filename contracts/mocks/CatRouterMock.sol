// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "../PolycatInterfaces.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract CatRouterMock is CatRouter {
    IERC20 public fishToken;
    IERC20 public pawToken;

    constructor(address _fishToken, address _pawToken) {
        fishToken = IERC20(_fishToken);
        pawToken = IERC20(_pawToken);
    }

    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts) {
        require(block.timestamp <= deadline);

        uint[] memory amountsOut = getAmountsOut(amountIn, path);
        require(amountsOut[1] >= amountOutMin);

        pawToken.transferFrom(msg.sender, address(this), amountIn);
        fishToken.transfer(to, amountsOut[1]);

        return amountsOut;
    }

    function getAmountsOut(
        uint amountIn,
        address[] memory /*path*/
    ) public view returns (uint[] memory amounts) {
        uint[] memory data = new uint[](2);
        data[0] = amountIn;
        data[1] = fishToken.balanceOf(address(this));
        return data;
    }
}
