// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

abstract contract VaultChef {
    // Info of each user.
    struct UserInfo {
        uint256 shares; // How many LP tokens the user has provided.
    }

    mapping(uint256 => mapping(address => UserInfo)) public userInfo; // Info of each user that stakes LP tokens.

    function deposit(uint256 _pid, uint256 _wantAmt) external virtual;

    function withdraw(uint256 _pid, uint256 _wantAmt) external virtual;

    function withdrawAll(uint256 _pid) external virtual;
}

interface StrategyDividends {
    function harvest() external;

    function pendingPaw(address _user) external view returns (uint256);
}

interface CatRouter {
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);

    function getAmountsOut(uint amountIn, address[] memory path)
        external
        view
        returns (uint[] memory amounts);
}
