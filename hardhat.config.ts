import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";

const config: HardhatUserConfig = {
  solidity: "0.8.17",
  networks: {
    frame: {
      url: "http://127.0.0.1:1248",
    },
    hardhat: {
      chainId: 1337,
    },
  },
};

export default config;
