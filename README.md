# Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, and a script that deploys that contract.

Try running some of the following tasks:

```shell
npx hardhat help
npx hardhat test
REPORT_GAS=true npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.ts
```
# Setup steps

To setup this project, the following commands have been run with pnpm:

```bash
pnpm --version # 7.16.0, allows the following setting to work
pnpm config set auto-install-peers true # Allow to automatically install peer dependencies of hardhat-toolbox
pnpm init
pnpm add hardhat
pnpm exec hardhat # Chose "Typescript project" and create a .gitignore
pnpm add typescript ts-node @nomicfoundation/hardhat-toolbox
```

After that, the command `pnpm hardhat test` should work.
